package msu

import java.io.File
import java.nio.charset.StandardCharsets

import scala.util.Random

/**
 * Copyright © makkarpov, 2015
 * Creation date: 05.10.15
 */
object AbelGenerator extends BaseGenerator {
  sealed abstract class MessagePart
  case class Plaintext(s: String) extends MessagePart {
    override def toString = s"""Plaintext("$s")"""
  }

  case class Ciphertext(data: Array[Byte]) extends MessagePart {
    override def toString = s"Ciphertext(${data.map("%02X" format _).mkString(" ")})"
  }

  case class Noise(bitLength: Int) extends MessagePart {
    override def toString = s"Noise($bitLength bit${if (bitLength > 1) "s" else ""})"
  }

  case class RawData(data: Array[Byte]) extends MessagePart {
    override def toString = s"RawData(${data.map("%02X" format _).mkString(" ")})"
  }

  val RandGen = new Random
  val HeaderBytes = "SECRET".getBytes(StandardCharsets.US_ASCII)
  val FooterBytes = "STOP".getBytes(StandardCharsets.US_ASCII)
  val TestTarget = new File("tests")
  var TestCount = 0

  def partEncrypt(state: Byte, data: Plaintext): (Byte, Ciphertext) = {
    val utf8 = data.s.getBytes(StandardCharsets.UTF_8)
    var st = state

    for (i <- utf8.indices) {
      val old = utf8(i)
      utf8(i) = (utf8(i) ^ st).toByte
      st = (old ^ st).toByte
    }

    (st, Ciphertext(utf8))
  }
  
  def messageEncrypt(parts: Seq[MessagePart]): Seq[MessagePart] = {
    val ret = Seq.newBuilder[MessagePart]
    var cipher: Byte = 0

    parts.foreach {
      case p: Noise => ret += p
      case p: RawData => ret += p
      case p: Ciphertext => ret += p
      case p: Plaintext =>
        val (cs, ct) = partEncrypt(cipher, p)

        cipher = cs
        ret += ct
    }

    ret.result()
  }

  def messageCreate(parts: Seq[MessagePart]): Array[Byte] = {
    val bitLength = parts.map {
      case Ciphertext(data) => 8 * (data.length + HeaderBytes.length + FooterBytes.length)
      case Noise(len)       => len
      case RawData(data)    => data.length * 8
    }.sum

    val bitstream = new Array[Byte](bitLength)
    var bitIndex = 0

    def placeBytes(data: Array[Byte]*): Unit = for (datum <- data; d <- datum) {
      for (m <- Seq(0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01)) {
        bitstream(bitIndex) = if ((d & m) != 0) 1 else 0
        bitIndex += 1
      }
    }

    def placeNoise(len: Int): Unit = for (i <- 0 until len) {
      bitstream(bitIndex) = if (RandGen.nextBoolean()) 1 else 0
      bitIndex += 1
    }

    parts.foreach {
      case Ciphertext(data) => placeBytes(HeaderBytes, data, FooterBytes)
      case RawData(data)    => placeBytes(data)
      case Noise(len)       => placeNoise(len)
    }

    assert(bitIndex == bitstream.length)

    val ret = new Array[Byte]((bitstream.length / 8.0F).ceil.toInt)
    for (i <- bitstream.indices if bitstream(i) != 0) {
      val m = 1 << (7 - i % 8)
      val j = i / 8

      ret(j) = (m | ret(j)).toByte
    }

    ret
  }

  def hexdump(data: Array[Byte]): String = {
    val sb = new StringBuilder
    
    for (i <- data.indices) {
      if (i != 0) {
        if (i % 16 == 0) sb.append('\n')
        else sb.append(' ')
      }

      sb.append("%02X" format (data(i) & 0xFF))
    }

    sb.toString()
  }

  def makeTest(comment: Option[String] = None, message: Seq[MessagePart], lengthOffset: Int = 0): Unit = {
    val binary = messageCreate(messageEncrypt(message))
    writeTest(
      meta = TestMetadata(userComment = comment),
      input = s"${binary.length + lengthOffset}\n${hexdump(binary)}",
      answer = {
        val r = message.collect{ case Plaintext(s) => s }

        if (r.nonEmpty) s"present\n${r.mkString("")}\n"
        else s"absent\n"
      }
    )
  }

  override def generate(): Unit = {
    makeTest(
      comment = Some("Один блок SECRET..STOP без каких-либо сдвигов"),
      message = Seq(
        Plaintext("H14 - Agent Abel")
      )
    )

    makeTest(
      comment = Some("SECRET STOP без текста сообщения"),
      message = Seq(Plaintext(""))
    )

    makeTest(
      comment = Some("\\0 в шифрованном тексте"),
      message = Seq(
        Plaintext("AABBCCDD")
      )
    )

    makeTest(
      comment = Some("Раннее окончание буфера"),
      message = Seq(
        Noise(16), RawData(HeaderBytes)
      ),
      lengthOffset = -1
    )

    makeTest(
      comment = Some("Один блок SECRET..STOP с длинным текстом, тест расшифровки"),
      message = Seq(
        Plaintext("Однажды в студеную зимнюю пору\nСижу за решеткой в темнице сырой\nГляжу, поднимается медленно в гору\nВскормленный в неволе орел молодой")
      )
    )

    makeTest(
      comment = Some("Несколько блоков SECRET..STOP, следующих друг за другом"),
      message = Seq(
        Plaintext("Есть два стула"), Plaintext(", на одном "), Plaintext("- Баула")
      )
    )

    makeTest(
      comment = Some("Текст, сдвинутый на 4 бита"),
      message = Seq(
        Noise(4), Plaintext("Linux ej.makkarpov.ru 3.11.10-cher1 #1 SMP Sat Dec 7 23:27:00 MSK 2013 i686 i686 i386 GNU/Linux"), Noise(4)
      )
    )

    makeTest(
      comment = Some("512 бит шума"),
      message = Seq( Noise(512) )
    )

    makeTest(
      comment = Some("Секрет без стопа - игнорируем неполный блок"),
      message = Seq(
        Noise(64), RawData("SECRETxxxx".getBytes)
      )
    )

    makeTest(
      comment = Some("SECRET...STOP + SECRET... - снова игнорируем неполный блок"),
      message = Seq(
        Noise(64), Plaintext("This is expected to be printed"), Noise(32),
        RawData("SECRETxxxx".getBytes)
      )
    )

    makeTest(
      message = Seq(
        Noise(5), Plaintext("New blood j"), Noise(3), Plaintext("oins this ea"),
        Noise(17), Plaintext("rth"), Noise(7), Plaintext("\nAnd quickly"), Noise(1),
        Plaintext(" he subdued"), Noise(12)
      )
    )

    makeTest(
      message = Seq(
        Noise(3), Plaintext("Per"), Noise(7), Plaintext(""), Noise(4), Plaintext("l is the o"),
        Noise(1), Plaintext("nly lan"), Noise(3), Plaintext("guage that l"), Noise(11),
        Plaintext("ooks the same be"), Noise(7), Plaintext("fore and af"), Noise(3),
        Plaintext("ter RSA encr"), Noise(7), Plaintext("yption."), Noise(31)
      )
    )
  }
}