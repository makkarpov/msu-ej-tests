package msu

import java.io.{OutputStreamWriter, FileOutputStream, OutputStream, File}
import java.nio.charset.StandardCharsets

/**
 * Copyright © makkarpov, 2015
 * Creation date: 19.10.15
 */
abstract class BaseGenerator {
  def directoryName: String = getClass.getSimpleName
      .replaceAll("\\$$", "")
      .replaceAll("Generator", "")
      .replaceAll("([a-z])([A-Z])", "$1_$2")
      .toLowerCase

  def testDirectory = new File(s"tests/$directoryName")

  var testNumber = 1

  def writeTest(meta: TestMetadata)(input: OutputStream => Unit)(output: OutputStream => Unit): Unit =
    writeTest(input)(output)(Some(meta))

  def writeTest(input: OutputStream => Unit)(output: OutputStream => Unit)(meta: Option[TestMetadata]): Unit = {
    val num = testNumber
    testNumber += 1

    val datStream = new FileOutputStream(new File(testDirectory, f"$num%03d.dat"))
    try input(datStream) finally datStream.close()

    val ansStream = new FileOutputStream(new File(testDirectory, f"$num%03d.ans"))
    try output(ansStream) finally ansStream.close()

    for (mt <- meta) {
      val infStream = new FileOutputStream(new File(testDirectory, f"$num%03d.inf"))
      try infStream <<: mt finally infStream.close()
    }
  }

  def writeTest(meta: TestMetadata, input: String, answer: String): Unit =
    writeTest(meta)(is => {
      val ow = new OutputStreamWriter(is, StandardCharsets.UTF_8)
      ow.write(input)
      ow.close()
    })(as => {
      val ow = new OutputStreamWriter(as, StandardCharsets.UTF_8)
      ow.write(answer)
      ow.close()
    })

  def generate(): Unit

  def main(args: Array[String]): Unit = {
    testDirectory.mkdirs()
    generate()
  }
}