package msu

import java.io.{OutputStreamWriter, PrintWriter, OutputStream}
import java.nio.charset.StandardCharsets

/**
 * Copyright © makkarpov, 2015
 * Creation date: 19.10.15
 */
case class TestMetadata(userComment: Option[String] = None, judgeComment: Option[String] = None,
                        commandLine: Option[Seq[String]] = None, environment: Option[String] = None,
                        exitCode: Option[Int] = None, disableStderr: Option[Boolean] = None,
                        checkStderr: Option[Boolean] = None) {

  private def writeString(pw: PrintWriter)(k: String, v: String): Unit = {
    pw.print(k)
    pw.print(" = ")
    pw.print(v)
    pw.println("")
  }

  private def escape(s: String): String = s.replaceAll("\\\\", "\\\\\\\\").replaceAll("\\\"", "\\\\\"")

  def <<:(os: OutputStream): Unit = {
    val pw = new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))

    userComment.foreach(x => writeString(pw)("teamComment", "\"" + escape(x) + "\""))
    judgeComment.foreach(x => writeString(pw)("comment", "\"" + escape(x) + "\""))
    commandLine.foreach(x => writeString(pw)("params", x.map("\"" + escape(_) + "\"").mkString(" ")))
    environment.foreach(writeString(pw)("environ", _))

    exitCode.foreach(x => writeString(pw)("exit_code", x.toString))
    disableStderr.foreach(x => writeString(pw)("disable_stderr", if (x) "1" else "0"))
    checkStderr.foreach(x => writeString(pw)("check_stderr", if (x) "1" else "0"))

    pw.println()
    pw.close()
  }
}
